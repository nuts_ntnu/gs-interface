
drop database if exists nutsdb;
create database if not exists nutsdb;

grant all on `<dbname>`.* to '<dbusername>'@'<dbhost>' identified by '<dbpasswd>';
flush privileges;
use nutsdb;

drop table if exists command_history;
create table if not exists command_history
(
	id         int primary key auto_increment,
	data       mediumblob not null,
	timestamp  datetime not null
);
