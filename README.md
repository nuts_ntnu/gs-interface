# Repo for EiT project #
Creating a ground station graphical user interface for (first off) telemetry data.

## Specifications ##
See the requirements.txt file.
Using **[Python 3.5](https://www.python.org/downloads/)**

## Automatic install ##

An automatic install will use template for the database and generate
a unique password for each installation and initialize needed tables.
It will compile the CSP library and the CSP module code. If the script fails it due to non-executable files or unmet dependencies. Read the output and fix it.

Debian-based linux distros can use the following script to install all the requirements:
```
./install.sh install
```

To clean installation run:
```
./install clean
```

## Install your database of choice ##

We have been using MySQL, which can be installed with your package manager. If you are using a database other than MySQL, you need to change that in the project config.

## Install necessary Python packages ##
Assuming you have Python 3.5 and pip installed and have cloned the repo, install the nescessary Python packages by running
```
pip install -r requirements.txt

```

## Make the necessary additions to your database ##

Add the user specified in the django config

## Run the web page ##
Type the following to run the web page through the port 8000
```
python manage.py runserver localhost:8000
```
Remember to install the pip package **django-bootstrap3==6.2.2** to display bootstrap stuff.

## How to use the software ##

1. Log in to Django admin with the superuser created during installation setup
2. Define the format of the messages which are to be received in the `MessageFormat` model
3. Define the values of the commands you will be using in the `Command` model
4. Go to the front page, type a command in the textfield, and refresh while waiting for a message to appear

## Code convention ##
We follow [NUTS' code standard](https://www.ntnu.no/wiki/display/nuts/Code+standard) and [PEP 0008 -- Style Guide for Python Code](https://www.python.org/dev/peps/pep-0008/).
