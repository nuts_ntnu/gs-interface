from django.conf.urls import url
from django.contrib import admin
from nuts_messages import views


urlpatterns = [
    url(r'^$', views.status, name='satellite-status'),
    url(r'^admin/', admin.site.urls),
]
