# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

# Using SQLite 3 during development is simpler. To use it, swap the
# DATABASE settings below.

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#     }
# }


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': '<dbname>',
        'USER': '<dbusername>',
        'PASSWORD': '<dbpasswd>',
        'HOST': '<dbhost>',
        'PORT': '',
    }
}
