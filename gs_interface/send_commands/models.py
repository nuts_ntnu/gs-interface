from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


class Command(models.Model):
    """Valid command to send to the satellite."""
    name = models.CharField(
        max_length=200,
        unique=True,
        help_text='Command phrase to execute in command line.',
    )
    command = models.PositiveIntegerField(
        unique=True,
        help_text='Command value corresponding to the desired action. \
        Must be a positive integer.',
        validators=[MaxValueValidator(255), MinValueValidator(0)],
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['command']
