from django.contrib import admin
from .models import Command


class CommandAdmin(admin.ModelAdmin):
    """Information on what to display and how in command admin page."""

    list_display = ['name', 'command', ]
    list_filter = ['name', 'command', ]

admin.site.register(Command, CommandAdmin)
