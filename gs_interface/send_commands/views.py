import os
import sys

from .models import Command
from .forms import CommandForm


def command_line_input(request):
    """Return context from command form."""

    feedback = None
    cmd_sent = None
    form = CommandForm(request.POST or None)

    if form.is_valid() and request.method == 'POST':
        cmd = request.POST.get('command')
        feedback = send_command_on_pipe(cmd)
        if not feedback:
            cmd_sent = Command.objects.filter(name=cmd)[0]

    return {'form': form, 'feedback': feedback, 'cmd_sent': cmd_sent}


def send_command_on_pipe(input_name):
    """Send entered command via the pipe that the CSP module listens to."""

    feedback = None

    try:
        # Open write only pipe with nonblock flag
        pipe = os.open('cmdpipe', os.O_WRONLY | os.O_NONBLOCK)

        # Return the command integer
        cmd_int = Command.objects.get(name=input_name).command
        os.write(pipe, bytes(bytearray([cmd_int])))

        # Close
        os.close(pipe)

    except OSError:
        feedback = 'ERROR: Can not send to the CSP module, there was a \
        problem with the pipe.'
    except:
        feedback = 'ERROR: ' + sys.exc_info()[1].strerror

    return feedback
