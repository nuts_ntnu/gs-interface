from django.apps import AppConfig


class SendCommandsConfig(AppConfig):
    name = 'send_commands'
