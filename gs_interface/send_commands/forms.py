from django import forms
from send_commands.models import Command


class CommandForm(forms.Form):
    """Command input form."""

    command = forms.CharField(
        label='',
        max_length=200,
    )

    def clean(self):
        """Check form input and raise eventual errors."""

        try:
            data = self.cleaned_data['command']
            if not Command.objects.filter(name=data):
                raise forms.ValidationError("ERROR: Command not defined.")
            return data
        except (KeyError, ):
            pass
