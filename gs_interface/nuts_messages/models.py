from django.db import models


class MessageFormat(models.Model):
    """Format of messages received from satellite. E.g. simple telemetry
    data.
    """
    name = models.CharField(
        max_length=200,
        help_text='Name of message format.',
    )
    message_id = models.IntegerField(
        help_text='Identification of message type.',
    )

    def __str__(self):
        return self.name


class Message(models.Model):
    """Format of a general message received from satellite."""

    data = models.BinaryField(
        max_length=64,
        help_text='String of bytes recevied from satellite.',
    )
    timestamp = models.DateTimeField(
        help_text='Time when byte string from satellite is entered into \
        the database.',
    )

    def __str__(self):
        return str(self.timestamp)

    class Meta:
        ordering = ['-timestamp']


class MessageField(models.Model):
    """Individual data/byte(s) contained within a message byte string."""

    name = models.CharField(
        max_length=200,
        help_text='Name of data point in database.',
    )
    function = models.CharField(
        max_length=200,
        help_text='Operation to execute on message byte. Refer to byte \
        as \'byte\', e.g. \'byte ** 2\'.',
        blank=True,
    )
    index = models.IntegerField(
        help_text='Database index. Zero indexing.',
    )
    unit = models.CharField(
        max_length=10,
        help_text='Unit of byte value.',
        blank=True,
    )
    message_format = models.ForeignKey(MessageFormat, )
    length = models.IntegerField(
        help_text='Length of message field in bytes.',
    )

    def __str__(self):
        return str(self.index) + ' ' + self.name

    class Meta:
        ordering = ['index']
