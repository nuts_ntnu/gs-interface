from django import template
from nuts_messages.views import dyn_display

register = template.Library()


@register.assignment_tag
def convert(message):
    """Convert bytestring to calculated values and return to template."""
    return dyn_display(message)
