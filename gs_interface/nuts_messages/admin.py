from django.contrib import admin
from .models import MessageFormat, MessageField


class MessageFieldInline(admin.StackedInline):
    model = MessageField
    extra = 0


class MessageFormatAdmin(admin.ModelAdmin):
    inlines = [MessageFieldInline, ]


admin.site.register(MessageFormat, MessageFormatAdmin)
