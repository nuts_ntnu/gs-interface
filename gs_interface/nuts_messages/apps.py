from django.apps import AppConfig


class NutsMessagesConfig(AppConfig):
    name = 'nuts_messages'
