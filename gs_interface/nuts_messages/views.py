from django.shortcuts import render
from .models import *
from send_commands.views import *


def status(request):
    """Filter messages by user input.
    :param request:
    :return: request, template and template context
    """

    query = 10
    e = []
    number = None

    # Check user input
    if ('f' in request.GET):
        try:
            number = int(request.GET['f'])
            assert number > 0
            query = number
        except:
            e = 'You did not enter a valid integer. Please do.'

    messages = Message.objects.all()[:query]
    if (len(messages) < query) and (number is not None):
        e = 'You asked for ' + str(number) \
            + ' message(s), while there is only ' + str(len(messages)) \
            + ' available.'

    send_commands_context = command_line_input(request)
    status_context = {'messages': messages, 'query': query, 'error': e}

    return render(request, template_name='base.html',
                  context={**status_context, **send_commands_context}, )


def dyn_display(msg):
    """Function for dynamic processing of messages, using the message
    format defined in django admin.
    :param msg: bytes
    :param msg_type: int or byte
    :return: list of fields in the message
    """
    message = {}
    counter = 0

    msg_type = msg.data[0]
    msg_format = None
    for msg_format_temp in MessageFormat.objects.all():
        if msg_format_temp.message_id == msg_type:
            msg_format = msg_format_temp

    msg_fields = MessageField.objects.filter(message_format=msg_format)

    # Get all the fields in the model
    # model_fields = MessageFormat._meta.get_all_field_names()

    # Find length of standard fields using model
    length_standard = 2
    if msg_format is not None:
        message[counter] = ("Message type", msg_format.name)
    counter += 1
    # TODO: Get proper byte from message
    message[counter] = ("Timestamp", msg.data[1])
    # for i in range(length_standard):
    #    #Find the correct field
    #    field_name = None
    #    #Get the data
    #    data = None
    #    message.append((field_name,data))
    # TODO: Change to length of message
    for j in range(length_standard, length_standard + len(msg.data)):
        # Find the correct field
        # field = msg_fields.find(index = j)
        field = None
        for messagefield in msg_fields:
            if messagefield.index == j:
                field = messagefield
        if not field:
            continue
        counter += 1

        # Get the data
        byte = msg.data[j]  # TODO: Get proper byte from message
        # Read more bytes than one

        # TODO: Use a documented eval() expression to apply the function
        try:
            byte = eval(messagefield.function)
        except (ImportError, SyntaxError):
            pass

        message[counter] = (field.name, byte, field.unit)

    return message
