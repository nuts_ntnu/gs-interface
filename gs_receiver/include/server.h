#ifndef server_h
#define server_h

#include <csp/csp.h>
#include <csp/arch/csp_thread.h>
#include "csp_object.h"
#include <mysql.h>


#define BUF_SIZE    250 // CSP FIFO buffer size




extern  csp_iface_t  csp_if_fifo;

/*---------------------------------------------------------------------------*/

/**
 * Handle a received packet from client
 */
void    handle_packet(csp_packet_t* packet);

/*---------------------------------------------------------------------------*/

/**
 * Define server thread function
 */
        CSP_DEFINE_TASK(task_server);
		
/**
 * Initilize and run the server thread
 * csp_object contains necessary pointers and variables
 * cmd_pipe_name: name of FIFO from which commands are read
 * mysql_conn: yeah...
 */		
int     run_loopback_server(
	                    struct csp_object* cspo,
	                    const char* cmd_pipe_name,
	                    MYSQL* mysql_conn);

/*---------------------------------------------------------------------------*/

/**
 * FIFO interface, not used but supported 
 */

int     run_fifo_server(struct csp_fifo_object* cspfo);

int     csp_fifo_tx(csp_iface_t *ifc, csp_packet_t *packet, uint32_t timeout);

void*   fifo_rx(void* parameters);

#endif
