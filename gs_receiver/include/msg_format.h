/* file: msg_format.h
*  contains: example of message format that can be used for testing by the EiT group of 2016
*  This file describes the PAYLOAD field in a csp/nrp package
*  Created: 21.02.2016 by Normann
*/
#ifndef msg_format_h
#define msg_format_h

#include <stdint.h>

#define PAYLOAD_SIZE_MAX	255

/* All telemetry transmitted to the ground will have two common byte fields:
*  byte 0: MSG_TYPE
*  byte 1: TIMETAG
*/

/* List of Telemetry Message Types */
#define 	PACK_PING 	 	0x01
#define 	TM_SIMPLE  		0x21
#define 	TM_EXTENDED 	0x22
#define 	TM_EPS 			0x31
#define 	TM_ADCS 		0x41
#define 	TM_PAYLOAD 		0x51
#define 	TM_OBC			0x61

/* Defines for each bit field of a TM_SIMPLE message */
typedef enum {
	MSG_TYPE,			// byte 0
	TIMETAG,			// byte 1
	UPTIME,				// byte 2
	BATT1_VOLT_BATT,	// byte 3
	BATT2_VOLT_BATT,	// byte 4
	BATT1_CURR_BATT,	// byte 5
	BATT2_CURR_BATT,	// byte 6
	EPS_CRG_VOLT_1,		// byte 7
	EPS_CRG_VOLT_2,		// byte 8
	EPS_CRG_VOLT_3,		// byte 9
	EPS_CRG_VOLT_4,		// byte 10
	EPS_CRG_VOLT_5,		// byte 11
	EPS_CRG_CURR_1,		// byte 12
	EPS_CRG_CURR_2,		// byte 13
	EPS_CRG_CURR_3,		// byte 14
	EPS_CRG_CURR_4,		// byte 15
	EPS_CRG_CURR_5,		// byte 16
	ADCS_SPIN_X,		// byte 17
	ADCS_SPIN_Y,		// byte 18
	ADCS_SPIN_Z,		// byte 19
	N_BYTES_TM_SIMPLE	// NB: Not a part of the package
} tm_simple_bit;


/* Example payload to use with a csp package */
//uint8_t tm_simple_example[N_BYTES_TM_SIMPLE] = {TM_SIMPLE,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};


#endif
