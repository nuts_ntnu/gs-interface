#ifndef net_h
#define net_h

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>


/**
 * Self explanatory
 */
struct tcp_object {
    struct sockaddr_in  addr;
    socklen_t           addrlen;
    int                 socket;
};


int tcp_open_server(
    struct tcp_object* server,
    const char* bind_addr,
    int port);


int tcp_close_server(
    struct tcp_object* server);


int tcp_accept(
    struct tcp_object* server,
    struct tcp_object* client);


int tcp_read(
    struct tcp_object* src,
    struct tcp_object* dest,
    char* buf,
    int len);


int tcp_write(
    struct tcp_object* src,
    struct tcp_object* dest,
    const char* buf,
    int len);


/**********************************************************
 * Please reconsider the choice of UDP instead of TCP.    *
 * Is UDP really the correct choice here?                 *
 * But if you still really want to use UDP... here you go *
 **********************************************************/

 struct udp_object {
 	struct sockaddr_in  addr;
 	socklen_t           addrlen;
 	int                 socket;
 };


/**
 * If this program is the server it listens for
 * incoming UDP packets before it can send back
 */
int udp_open_server(
	struct udp_object* server,
	const char* bind_addr,
	int port);

/**
 * Close UDP socket
 */
int udp_close_server(
	struct udp_object* server);

/**
 * If this program is the client it sends UDP
 * packets before it can receive.
 */
int udp_client_setup(
	struct udp_object* server,
	struct udp_object* client,
	const char* host_addr,
	int port);


/**
 * Read incoming packets
 * from src to dest
 */
int udp_read(
	struct udp_object* src,
	struct udp_object* dest,
	char* buf,
	int len);

/**
 * Write packets
 * from src to dest
 */
int udp_write(
	struct udp_object* src,
	struct udp_object* dest,
	const char* buf,
	int len);



#endif
