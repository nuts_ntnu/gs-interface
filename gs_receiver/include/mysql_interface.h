#ifndef mysql_interface_h
#define mysql_interface_h

#include <stdint.h>
#include <mysql.h>
#include <my_global.h>

MYSQL* init_mysql_db(const char* host, const char* username, const char* passwd, const char* db);
void   create_table(MYSQL* conn, char* table_name);
void   write_msg_to_db(MYSQL* conn, const char* table, const char* msg_buf, size_t size);
void   close_mysql(MYSQL* conn);

#endif
