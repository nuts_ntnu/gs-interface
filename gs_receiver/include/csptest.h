#ifndef csptest_h
#define csptest_h

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <csp/csp.h>
#include <csp/arch/csp_thread.h>

#define MY_ADDRESS  1			// Address of local CSP node
#define MY_PORT		10			// Port to send test traffic to

CSP_DEFINE_TASK(task_server);
CSP_DEFINE_TASK(task_client);

int test_local_csp(int argc, char* argv[]);

#endif
