#ifndef msggen_h
#define msggen_h

/**
 * Strict message generator
 */
int     msggen(char* buf, int length);

/**
 * Random message generator
 */
int     msggenrnd(char* buf, int length);


#endif
