#ifndef client_h
#define client_h

#include <csp/csp.h>
#include <csp/arch/csp_thread.h>
#include "csp_object.h"

/**
 * The client should be the satellite, and therefore 
 * this component is not needed for anything but testing the
 * server implementation.
 *
 * This component emulates a satellite for communiation by
 * generating random satellite data and sends them to the server.
 * After each send it tries to read a command from the server.
 * Received commands are only printed out to show that they are
 * received, nothing else is done.
 */




/**
 * Defines a thread task for the client emulation
 */
CSP_DEFINE_TASK(task_client);

/**
 * Initializes the client. Creates a thread, and starts it.
 * Message interval indicates the amount of milliseconds to sleep 
 * before generating a new message and sending it to the server.
 */
int run_loopback_client(struct csp_object* cspo, unsigned msginterval);

#endif
