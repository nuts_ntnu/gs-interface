#ifndef csp_object_h
#define csp_object_h

#include <csp/csp.h>
#include <csp/csp_interface.h>
#include <csp/arch/csp_semaphore.h>


struct csp_object {
    int            host_addr;    // CSP host address
    int            client_addr;  // CSP client address        
    int            port;         // CSP port
    int            buffer_size;  // CSP buffer size      
    csp_mutex_t*   mutex;        // CSP mutex
         
    csp_socket_t*  sock;         // CSP socket
    csp_conn_t*    conn;         // CSP connection
};

struct csp_fifo_object {
    struct csp_object* cspo;     // CSP object

    // fifo channels
    char*            rx_channel_name; // CSP fifo interface receive channel 
    char*            tx_channel_name; // CSP fifo interface transmit channel
};


#endif
