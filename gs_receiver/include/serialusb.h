#ifndef serialusb_h
#define serialusb_h


int read_usbdata(FILE* in, char* buffer, size_t count);
int store_usbdata(FILE* out, char* buffer, size_t count);


#endif
