#include "util.h"
#include "stdio.h"

void printbuffer(char* buf, int count)
{
    printf(PADDING);
    for (int i = 0; i < count; i++) {
        printf("%.2x ", (unsigned)(unsigned char)buf[i]);
    }
    printf("\n");
    fflush(stdout);
}
