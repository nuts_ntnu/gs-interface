#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <csp/csp.h>
#include <csp/csp_interface.h>
#include <csp/arch/csp_thread.h>

#include "msggen.h"
#include "client.h"
#include "csp_object.h"
#include "util.h"

static unsigned msg_interval = 10000;

#define CLICOLOR "\x1b[38;5;38m"
#define CLIPREFIX CLICOLOR "[CLIENT] "


/**
 * Client does nothing with the packet.
 * Prints out notification of received packet and
 * the received data
 */
static void handle_packet(csp_packet_t* packet)
{
	printf(CLIPREFIX COLOR_GREEN "Received msg %d bytes:\n" COLOR_RESET, packet->length);
	printbuffer(packet->data, packet->length);
}


int run_loopback_client(struct csp_object* cspo, unsigned msginterval)
{
	msg_interval = msginterval;
	csp_thread_handle_t handle_client;
	csp_thread_create(task_client, "CLIENT", 1000, cspo, 0, &handle_client);
	return 0;
}



CSP_DEFINE_TASK(task_client) {

	struct csp_object* cspo = (struct csp_object*)param;

	csp_packet_t* packet;
	csp_packet_t* cmdpacket;

	while (1) {
		csp_sleep_ms(msg_interval);
//		int result = csp_ping(cspo->host_addr, 100, 100, CSP_O_NONE);
//		printf("Ping result %d [ms]\r\n", result);
//		csp_sleep_ms(1000);

		packet = csp_buffer_get(cspo->buffer_size);
		if (packet == NULL) {
			printf(CLIPREFIX COLOR_RED "Failed to get buffer element\n" COLOR_RESET);
			csp_sleep_ms(1000);
			continue;
		}

		printf("\n\n");
		printf(CLIPREFIX "Connecting to server...\n" COLOR_RESET);
		cspo->conn = csp_connect(CSP_PRIO_NORM, cspo->host_addr, cspo->port, 10000, CSP_O_NONE);
		if (cspo->conn == NULL) {
			printf(CLIPREFIX COLOR_RED "Connection failed\n" COLOR_RESET);
			csp_buffer_free(packet);
			csp_sleep_ms(1000);			
			continue;
		}

		msggenrnd(packet->data, 20);
		packet->length = 20;
		
		

		printf(CLIPREFIX "Sending msg %d bytes:\n" COLOR_RESET, packet->length);
		printbuffer(packet->data, packet->length);
		
		//printf("cspo->conn: %x   packet: %x\n", cspo->conn, packet);
		if (!csp_send(cspo->conn, packet, 1000)) {
			printf(CLIPREFIX COLOR_RED "Send failed\n" COLOR_RESET);
			csp_buffer_free(packet);
		}

	
		/* read commands */
		printf(CLIPREFIX "Waiting for commands ...\n" COLOR_RESET);
		while ((cmdpacket = csp_read(cspo->conn, 2000)) != NULL) {
			handle_packet(cmdpacket);
			csp_buffer_free(cmdpacket);
		}
		if (cspo->conn)
			csp_close(cspo->conn);
		
		printf(CLIPREFIX "CLOSED\n");
	}
	return CSP_TASK_RETURN;
}
