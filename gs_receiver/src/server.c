#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/select.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdint.h>

#include <csp/csp.h>
#include <csp/csp_interface.h>
#include <csp/arch/csp_thread.h>
#include <csp_conn.h>
#include <csp/csp_types.h>

#include "util.h"
#include "msggen.h"
#include "server.h"
#include "mysql_interface.h"
#include "csp_object.h"

#define SRVCOLOR "\x1b[38;5;127m"
#define SRVPREFIX SRVCOLOR "[SERVER] "

static pthread_t   rx_thread;
static int         rx_channel, tx_channel;
static int         server_out;
static MYSQL*      mysql_conn;
static char*       cmd_buffer;
static int         cmd_len;
static int         cmd_pipe_fd;

static void**      alloc2d(size_t rows, size_t cols, size_t datasize);
static int         open_nonblock(const char* filename);


/**
 * Handles packet received from client.
 * Displays received data.
 * Stores the data received into the database.
 */
void handle_packet(csp_packet_t* packet)
{
	printf(SRVPREFIX COLOR_GREEN "Received msg %d bytes:\n" COLOR_RESET, packet->length);
	printbuffer(packet->data, packet->length);

	write_msg_to_db(mysql_conn, "nuts_messages_message", packet->data, packet->length);
}


/**
 * Initializes the server with provided csp_object, cmd FIFO name and mysql connections
 */
int run_loopback_server(struct csp_object* cspo, const char* cmd_pipe_name, MYSQL* msqlc)
{
	mysql_conn = msqlc;
	cmd_pipe_fd = open(cmd_pipe_name, O_RDWR | O_NONBLOCK);

	cmd_buffer = calloc(cspo->buffer_size, sizeof(char));
	cmd_len = 0;
	csp_thread_handle_t handle_server;
	csp_thread_create(task_server, "SERVER", 1000, cspo, 0, &handle_server);
	
	return 0;
}



CSP_DEFINE_TASK(task_server) {
	struct csp_object* cspo = (struct csp_object*)param;

	/* create socket, bind it to port and start listening */
	cspo->sock = csp_socket(CSP_SO_NONE);
	csp_bind(cspo->sock, cspo->port);
	csp_listen(cspo->sock, 5); // max 5 awating clients in queue

	printf(SRVPREFIX "Waiting for connections ...\n" COLOR_RESET);
	while (1) {
		if ((cspo->conn = csp_accept(cspo->sock, 1000)) == NULL){		
			/* no connection */
			continue; 
		}
		
		
		printf(SRVPREFIX COLOR_GREEN "Client connected.\n" COLOR_RESET);
		csp_packet_t* packet;
		
		/* read */
		while ((packet = csp_read(cspo->conn, 500)) != NULL) {
			int port = csp_conn_dport(cspo->conn);
	
			/* packet received */
			if (port == cspo->port) {
				handle_packet(packet);
				csp_buffer_free(packet);
			}
			else {
				/* reply pings, buffer use, etc. */
				csp_service_handler(cspo->conn, packet);
			}
		}

		/* read and send available commands */
		cmd_len = read(cmd_pipe_fd, cmd_buffer, cspo->buffer_size);
		if (cmd_len > 0) {
			printf(SRVPREFIX "Sending cmd %d bytes:\n" COLOR_RESET, cmd_len);
			printbuffer(cmd_buffer, cmd_len);
			
			write_msg_to_db(mysql_conn, "command_history", cmd_buffer, cmd_len);

			csp_packet_t* cmdpacket = csp_buffer_get(cspo->buffer_size);
			if (cmdpacket == NULL) {
				printf(SRVPREFIX COLOR_RED "ERROR cmd packet\n"COLOR_RESET);
				return CSP_TASK_RETURN;
			}
			
			memcpy(cmdpacket->data, cmd_buffer, cmd_len);
			cmdpacket->length = cmd_len;

			if (!csp_send(cspo->conn, cmdpacket, 2000)) {
				printf(SRVPREFIX COLOR_RED "Send failed\n" COLOR_RESET);
				csp_buffer_free(cmdpacket);
			} else {
				csp_sleep_ms(2000);
			}
		}
		csp_close(cspo->conn);
		printf(SRVPREFIX "CLOSED\n");
	}
	
	free(cmd_buffer);
	return CSP_TASK_RETURN;
}



/**
 * Allocate 2D array
 */
void** alloc2d(size_t rows, size_t cols, size_t datasize)
{
	void** ptrs = malloc(rows * sizeof(void*));
	ptrs[0] = malloc(rows * cols * datasize);
	for (size_t i = 1; i < rows; i++) {
		ptrs[i] = ptrs[i-1] + cols * datasize;
	}
	return ptrs;
}



/*****************************************************************************/



csp_iface_t csp_if_fifo = {
    .name = "fifo",
    .nexthop = csp_fifo_tx,
    .mtu = BUF_SIZE,
};



int run_fifo_server(struct csp_fifo_object* cspfo)
{
    server_out = open("server_out", O_WRONLY | O_APPEND | O_CREAT, 0666);
    if (server_out < 0) {
        perror("[ERROR] Open file \"server_out\"");
    }

    csp_packet_t*    packet;


    /* Init CSP and CSP buffer system, buffer => 10 packets of maximum 256 bytes */
    if (csp_init(cspfo->cspo->host_addr) != CSP_ERR_NONE || csp_buffer_init(10, 256) != CSP_ERR_NONE) {
        printf("Failed to init CSP\r\n");
        return -1;
    }

    tx_channel = open(cspfo->tx_channel_name, O_RDWR);
    if (tx_channel < 0) {
        printf("Failed to open TX channel\r\n");
        return -1;
    }

    rx_channel = open(cspfo->rx_channel_name, O_RDWR);
    if (rx_channel < 0) {
        printf("Failed to open RX channel\r\n");
        return -1;
    }

    /* Start fifo RX task */
	pthread_create(&rx_thread, NULL, fifo_rx, NULL);

    /* Set default route and start router */
    csp_route_set(CSP_DEFAULT_ROUTE, &csp_if_fifo, CSP_NODE_MAC);
    csp_route_start_task(0, 0);

    cspfo->cspo->sock = csp_socket(CSP_SO_NONE);
    csp_bind(cspfo->cspo->sock, cspfo->cspo->port);
    csp_listen(cspfo->cspo->sock, 5);

    /* Super loop */
    while (1) {
        /* Process incoming packet */
        cspfo->cspo->conn = csp_accept(cspfo->cspo->sock, 1000);
        if (cspfo->cspo->conn) {
            packet = csp_read(cspfo->cspo->conn, 0);
			if (packet) {
                handle_packet(packet);
                int res = write(server_out, packet->data, packet->length);
				if (res <= 0)
                    perror("[ERROR] Writing to file");
            }
            csp_buffer_free(packet);
            csp_close(cspfo->cspo->conn);
        }
    }

    close(rx_channel);
    close(tx_channel);

    return 0;
}



int csp_fifo_tx(csp_iface_t* ifc, csp_packet_t* packet, uint32_t timeout)
{
    /* Write packet to fifo */
    int res = write(tx_channel, &packet->length, packet->length + sizeof(uint32_t) + sizeof(uint16_t));
    if (res < 0) {
        printf("Failed to write frame\r\n");
    }
    csp_buffer_free(packet);

    return CSP_ERR_NONE;
}



void* fifo_rx(void* parameters)
{
    csp_packet_t* packet = csp_buffer_get(BUF_SIZE);

	struct timeval timeout = {.tv_sec = 1, .tv_usec = 0}; // 1 sec timeout
	fd_set rfds;

    while (1){
		int inc = read(rx_channel, &packet->length, BUF_SIZE);
		if (inc > 0) {
        	csp_new_packet(packet, &csp_if_fifo, NULL);
        	packet = csp_buffer_get(BUF_SIZE);
		}
    }
    return NULL;
}
