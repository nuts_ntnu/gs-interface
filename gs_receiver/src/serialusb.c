#include "serialusb.h"
#include <stdio.h>


int open_usb_port(const char* path, int flags)
{
    return open(path, flags);
}


int read_usb_data(int fd, char* buffer, size_t count)
{
    return read(fd, buffer, count);
}
