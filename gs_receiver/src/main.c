#include <time.h>
#include <fcntl.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <getopt.h>

#include <csp/csp.h>
#include <csp/csp_interface.h>
#include <csp/arch/csp_thread.h>
#include <csp/arch/csp_semaphore.h>

#include "msggen.h"
#include "msg_format.h"
#include "server.h"
#include "client.h"
#include "net.h"
#include "mysql_interface.h"
#include "csp_object.h"
#include "util.h"



#define PACKET_LENGTH 20
static char          c;
static unsigned      msginterval  = 10000;
static char*         cmdpipe_name = "cmdpipe";
static int           verbose = 0;
static char*         dbname = NULL;
static char*         dbusername = NULL;
static char*         dbhost = NULL;
static char*         dbpasswd = NULL;

static struct option long_options[] = {
	{"verbose", no_argument,       &verbose, 1},
		
	{"fifo",       required_argument, 0, 'f'},	
	{"dbpasswd",   required_argument, 0, 'p'},	
	{"dbusername", required_argument, 0, 'u'},	
	{"dbhost",     required_argument, 0, 'h'},	
	{"dbname",     required_argument, 0, 'n'},	
	{"interval",   required_argument, 0, 'i'},	
	{"config",     required_argument, 0, 'c'},	
	{0,0,0,0}
};

static void help()
{
	printf("Invalid option.\n");
	printf("Arguments are:\n");
	printf("    [-i|--interval <message send interval in ms>]\n");
	printf("    [-f|--fifo <command pipe aka. fifo path>]\n");
	printf("    [-c|--config <command config path>]\n");
	printf("REQUIRED:\n");
	printf("    [-n|--dbname <database name>]\n");
	printf("    [-u|--dbuser <database username>]\n");
	printf("    [-p|--dbpasswd <database password>]\n");
	printf("    [-h|--dbhost <database host>]\n");
	printf("\n");
}


int main(int argc, char* const argv[])
{

	while (1) {
		int option_index = 0;
		c = getopt_long (argc, argv, "f:i:c:u:p:h:n:", long_options, &option_index);
		if (c == -1) break;
		
		switch (c) {
		case 'i':
			msginterval = atoi(optarg);
			break;
		case 'f':
			cmdpipe_name = optarg;
			break;
		case 'h':
			dbhost = optarg;
			break;
		case 'u':
			dbusername = optarg;
			break;
		case 'p':
			dbpasswd = optarg;
			break;
		case 'n':
			dbname = optarg;
			break;
		case '?':
			help();
		default:
			exit(1);
		}
	}
	if (dbhost == NULL || dbname == NULL || dbusername == NULL || dbpasswd == NULL) {
		printf(COLOR_RED "Missing database information.\n" COLOR_RESET);
		help();
		exit(1);
	}


	//                                 host         user    pw          db
	MYSQL* mysql_conn = init_mysql_db(dbhost, dbusername, dbpasswd, dbname);
	if (mysql_conn == NULL) {
		exit(2);
	}

	time_t t = time(NULL);
	srand((unsigned) time(&t));
	struct tm tm = *localtime(&t);
	printf("Started   %d-%d-%d %d:%d:%d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);


	struct csp_object cspo_srv = {
			.host_addr = 1,
			.client_addr = 2,
			.port = 10,
			.buffer_size = 256,
	};
	
	struct csp_object cspo_cli = {
			.host_addr = 1,
			.client_addr = 2,
			.port = 10,
			.buffer_size = 256,
	};

	printf("Initialising CSP\r\n");
	csp_buffer_init(10, cspo_srv.buffer_size);
	csp_init(cspo_srv.host_addr);
	csp_route_start_task(500, 1);
	
	csp_buffer_init(10,cspo_cli.buffer_size);
	csp_init(cspo_cli.host_addr);
	csp_route_start_task(500, 1);

	printf("Starting Server\n");
	run_loopback_server(&cspo_srv, cmdpipe_name, mysql_conn);
	run_loopback_client(&cspo_cli, msginterval);

	while (1) {
		csp_sleep_ms(1000000);
	}

	return 0;
}
