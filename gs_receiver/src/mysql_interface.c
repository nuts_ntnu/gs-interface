#include <stdio.h>
#include <stdlib.h>
#include <mysql.h>
#include <my_global.h>
#include <time.h>
#include <string.h>

#include "msg_format.h"
#include "mysql_interface.h"
#include "util.h"


/**
 * R.I.P program, something went horribly wrong...
 */
void finish_with_error(MYSQL *conn)
{
	fprintf(stderr, "%s\n", mysql_error(conn));
	mysql_close(conn);
	exit(1);
}


/**
 * Open database connection, nothing more nothing less
 */
MYSQL* init_mysql_db(const char* host, const char* username, const char* passwd, const char* db)
{
	MYSQL* conn = mysql_init(NULL);

	if (conn == NULL) {
		fprintf(stderr, "%s\n", mysql_error(conn));
		exit(1);
	}

	if (mysql_real_connect(conn, host, username, passwd, NULL, 0, NULL, 0) == NULL) {
		finish_with_error(conn);
	}

	if (mysql_select_db(conn, db) != 0) {
		finish_with_error(conn);
	}

	return conn;
}


/**
 * Let's create a table.
 * You provide the name, and after some magic,
 * a table has spawned.
 */
void create_table(MYSQL* conn, char* table_name)
{
	char* query = "CREATE TABLE";
	sprintf(query, "%s",table_name);

	if (mysql_query(conn, query)){
		finish_with_error(conn);
	}
}



/**
 * Sqeeze in binary data into the provided table.
 * Make sure the "size" is correct,
 * or else...
 */
void write_msg_to_db(MYSQL* conn, const char* table, const char* msg_buf, size_t size) // uint8_t* msg
{
	/* allocate enough space for compiled query */
	int querylen = size*2+512;
	char* query = calloc(querylen, 1);
	if (query == NULL) {
		perror("calloc");
		return;
	}

	/* allocate enough space for binary escape */
	char blob[size*2+1];

	/* additional data */
	time_t timestamp = time(NULL);

	char* stat = "INSERT INTO %s VALUES(default, '%s', NOW())";

	/* make the damn query, and yes this is vulnerable for SQL injection :-) */
	int encoded_len = mysql_real_escape_string(conn, blob, msg_buf, size);
	int len = snprintf(query, querylen, stat, table, blob, (unsigned)timestamp);

	int run = mysql_real_query(conn, query, len);
	free(query);

	if (run) {
		finish_with_error(conn);
	}
}


/* guess what... it closes the connection! wooow */
void close_mysql(MYSQL* conn)
{
	mysql_close(conn);
}
