#include "msggen.h"
#include "string.h"
#include "msg_format.h"
#include "util.h"
#include <stdio.h>
#include <stdlib.h>


/**
 * Strict message
 */
int msggen(char* buf, int length)
{
    int offset           = 0;

    char MSG_TYPE        = 0x0;
    char TIME_TAG        = 0x1;
    char UPTIME          = 0x2;
    char BATT1_VOLT_BATT = 0x3;
    char BATT2_VOLT_BATT = 0x4;
    char BATT1_CURR_BATT = 0x5;
    char BATT2_CURR_BATT = 0x6;

    #define len 7
    const char msg[] = {
        MSG_TYPE,
        TIME_TAG,
        UPTIME,
        BATT1_VOLT_BATT,
        BATT2_VOLT_BATT,
        BATT1_CURR_BATT,
        BATT2_CURR_BATT
    };

    uint8_t tm_simple_example[N_BYTES_TM_SIMPLE] = {TM_SIMPLE,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
    memcpy(buf, &(tm_simple_example[0]), N_BYTES_TM_SIMPLE);

    return 0;
}


/**
 * Random message
 */
int msggenrnd(char* buf, int length)
{
    buf[0] = TM_SIMPLE;
    for (int i = 1; i < 7; i++) {
        buf[i] = (rand() % 256);
    }
    for (int i = 7; i < 17; i++) {
        buf[i] = (rand() % 10);
    }
    for (int i = 17; i < length; i++) {
        buf[i] = (rand() % 90) + 10;
    }
}
