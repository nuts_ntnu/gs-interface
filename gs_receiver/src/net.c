#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "net.h"



int tcp_open_server(struct tcp_object* server, const char* bind_addr, int port)
{
	server->socket = socket(AF_INET, SOCK_STREAM, 0);
	if (server->socket < 0) {
		perror("tcp server socket");
		exit(1);
	}

	memset((char*)&server->addr, 0, sizeof(server->addr));
	server->addr.sin_family      = AF_INET;
	server->addr.sin_port        = htons(port);
	server->addr.sin_addr.s_addr = inet_addr(bind_addr);

	int sock = server->socket;
	struct sockaddr* addr = (struct sockaddr*)&server->addr;
	socklen_t addrlen = sizeof(server->addr);
	server->addrlen = addrlen;

	int binding = bind(sock, addr, addrlen);
	if (binding < 0) {
		perror("bind error");
		exit(1);
	}

	listen(server->socket, 15);
}


int tcp_close_server(struct tcp_object* server)
{
	close(server->socket);
}


int tcp_accept(struct tcp_object* server, struct tcp_object* client)
{
	struct sockaddr* cliaddr = (struct sockaddr*)&client->addr;
	socklen_t*       cliaddrlen = &client->addrlen;

	client->socket = accept(server->socket, cliaddr, cliaddrlen);
	return client->socket;
}


int tcp_read(
    struct tcp_object* src,
    struct tcp_object* dest,
    char* buf,
    int len)
{
	int sock = src->socket;

	struct sockaddr* src_addr = (struct sockaddr*)&src->addr;
	src->addrlen = sizeof src_addr;

	int recvlen = recvfrom(sock, buf, len, 0, src_addr, &src->addrlen);
	if (recvlen < 0) {
		perror("recv error");
	}
	return recvlen;
}


int tcp_write(
    struct tcp_object* src,
    struct tcp_object* dest,
    const char* buf,
    int len)
{
	int sock = dest->socket;

	struct sockaddr* dest_addr = (struct sockaddr*)&dest->addr;
	socklen_t dest_addrlen = sizeof(dest->addr);
	dest->addrlen = dest_addrlen;

	int sendres = sendto(sock, buf, len, 0, dest_addr, dest_addrlen);
	if (sendres < 0) {
		perror("send error");
	}
	return sendres;
}



/******************************************************************************/
/******************************************************************************/



int udp_open_server(struct udp_object* server, const char* bind_addr, int port)
{
	server->socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (server->socket < 0) {
		perror("udp server socket");
		exit(1);
	}

	memset((char*)&server->addr, 0, sizeof(server->addr));
	server->addr.sin_family      = AF_INET;
	server->addr.sin_port        = htons(port);
	server->addr.sin_addr.s_addr = inet_addr(bind_addr);

	int sock = server->socket;
	struct sockaddr* addr = (struct sockaddr*)&server->addr;
	socklen_t addrlen = sizeof(server->addr);
	server->addrlen = addrlen;

	int binding = bind(sock, addr, addrlen);
	if (binding < 0) {
		perror("bind error");
		exit(1);
	}
}


int udp_close_server(struct udp_object* server)
{
	close(server->socket);
}


int udp_read(
	struct udp_object* src,
	struct udp_object* dest,
	char* buf,
	int len)
{
	int sock = dest->socket;

	struct sockaddr* src_addr = (struct sockaddr*)&src->addr;
	src->addrlen = sizeof src_addr;

	int recvlen = recvfrom(sock, buf, len, 0, src_addr, &src->addrlen);
	if (recvlen < 0) {
		perror("recv error");
	}
	return recvlen;
}


int udp_write(
	struct udp_object* src,
	struct udp_object* dest,
	const char* buf,
	int len)
{
	int sock = src->socket;

	struct sockaddr* dest_addr = (struct sockaddr*)&dest->addr;
	socklen_t dest_addrlen = sizeof(dest->addr);
	dest->addrlen = dest_addrlen;

	int sendres = sendto(sock, buf, len, 0, dest_addr, dest_addrlen);
	if (sendres < 0) {
		perror("send error");
	}
	return sendres;
}



int udp_client_setup(
	struct udp_object* server,
	struct udp_object* client,
	const char* host_addr,
	int port)
{
	client->socket = socket(AF_INET, SOCK_DGRAM, 0);
	if (client->socket < 0) {
		perror("client socket");
		exit(1);
	}

	memset((char*)&server->addr, 0, sizeof(server->addr));
	server->addr.sin_family      = AF_INET;
	server->addr.sin_port        = htons(port);
	server->addr.sin_addr.s_addr = inet_addr(host_addr);
	socklen_t addrlen = sizeof(server->addr);

	return client->socket;
}
