#!/bin/bash


RED="\e[0;31m"
GRN="\e[0;32m"
YEL="\e[0;33m"
CYA="\e[0;36m"
RES="\e[0m"

PYSETTINGS="./gs_interface/gs_interface/dbsettings.py"
DBSCRIPT="./dbscript.sql"

RESDIR="$(pwd)"
DEPS="python3.5 pip3 gcc make mysql"



DBUSERNAME='nuts'
DBNAME='nutsdb'
DBPASSWD=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)
DBHOST='localhost'


function DEPS_check {
	echo -en "$CYA"
	echo -en "Checking dependencies ..."
	echo -e "$RES"
	for p in $DEPS; do
		type $p &>/dev/null
		if [[ $? -eq 0 ]]; then
			echo -e "$p $GRN OK. $RES"
		else
			echo -e "$p $RED NOT FOUND. $RES"
			exit 1
		fi
	done
}


function configure {
	sed -i "s/<dbname>/$DBNAME/g" $PYSETTINGS
	sed -i "s/<dbusername>/$DBUSERNAME/g" $PYSETTINGS
	sed -i "s/<dbpasswd>/$DBPASSWD/g" $PYSETTINGS
	sed -i "s/<dbhost>/$DBHOST/g" $PYSETTINGS

	sed -i "s/<dbname>/$DBNAME/g" $DBSCRIPT
	sed -i "s/<dbusername>/$DBUSERNAME/g" $DBSCRIPT
	sed -i "s/<dbpasswd>/$DBPASSWD/g" $DBSCRIPT
	sed -i "s/<dbhost>/$DBHOST/g" $DBSCRIPT
}


function install_db {
	echo "Input MySQL root user: "
	read rootuser
	
	mysql -u$rootuser -p  < $DBSCRIPT || rekt
}


function install_csp {
	echo -en "$CYA"
	echo -en "--- Building csp library ---"
	echo -e "$RES"
	if [[ ! -d "libcsp" ]]; then
		echo -en "$RED"
		echo -e  "ERROR: Missing libcsp ... Aborting! $RES"
		exit 1
	fi

	cd libcsp
	if [[ ! -d "libcsp-master" ]]; then
		if [[ ! -f "libcsp-master.zip" ]]; then
			echo "Missing libcsp-master.zip"
			exit 1
		fi
		echo -en "Inflating libcsp-master.zip ..."
		unzip libcsp-master.zip > /dev/null
		echo -e "$GRN OK $RES"
	fi

	cd libcsp-master
	./waf configure --with-driver-usart=linux --with-os=posix --enable-crc32 || rekt
	./waf build install || rekt
	echo -e "$RES"
}

function install_receiver {
	echo -en "$CYA"
	echo -en "--- Building receiver ---"
	echo -e "$RES"
	cd $RESDIR
	cd gs_receiver
	echo -en "$GRN"
	make all
	echo -e "$RES"
}

function install_web {
	echo -en "$CYA"
	echo -en "--- Buiding web interface ---" 
	echo -e "$RES"
	cd $RESDIR
	if [[ ! -f "requirements.txt" ]]; then
		echo -en "$RED"
		echo -e  "Missing requirements.txt... Aborting! $RES"
	fi
	pip3 install -r requirements.txt || rekt
	
	cd gs_interface

	if [[ ! -p "cmdpipe" ]]; then 
		mkfifo cmdpipe
	fi

	python3.5 manage.py makemigrations || rekt
	python3.5 manage.py makemigrations nuts_messages send_commands || rekt
	python3.5 manage.py migrate || rekt
	
	echo -en "$CYA"
	echo -e "Adding superuser: $RES"
	python3.5 manage.py createsuperuser || rekt

	cd $RESDIR
}


function clean {
	echo "Cleaning"
	cd $RESDIR
	rm -r libcsp/libcsp-master > /dev/null 2>&1
	cd gs_receiver
	make clean > /dev/null 2>&1
    cd $RESDIR
}


function run {
	if [[ "$1" == "web" ]]; then
		echo "Running web server"
		cd gs_interface
		python3.5 manage.py runserver localhost:8000
	elif [[ "$1" == "base" ]]; then
		echo "Running base station server"
		pass=$(settings_extract PASSWORD)
		cd gs_receiver		
		./basestation -f "../gs_interface/cmdpipe" -i10000 -n $DBNAME -u $DBUSERNAME -h $DBHOST -p $pass
	else
		echo "Invalid run option."
	fi
}


function settings_extract {
	var=$(cat $PYSETTINGS | grep -e "^\(\s*\)'$1'" | cut -d\' -f4)
	echo -en $var
}


function rekt {
	echo -en "$RED"
	echo -e "ERROR: $1"
	echo -e "Aborting.$RES"
	exit 1
}




###############################################################################


if [[ ! -f $PYSETTINGS ]]; then
	echo "Missing django settings file: $PYSETTINGS"
	exit 1
fi


if [[ $1 == "clean" ]]; then
	clean
elif [[ $1 == "run" ]]; then
	run $2
elif [[ $1 == "csp" ]]; then
	DEPS_check
	install_csp
elif [[ $1 == "install" ]]; then
	DEPS_check
	configure
	install_db
	install_csp
	install_receiver
	install_web
elif [[ $1 == "db" ]]; then
	DEPS_check
	configure
	install_db
else
	echo "Args: [ clean | install | run [web|base] | csp | db ]"
fi
